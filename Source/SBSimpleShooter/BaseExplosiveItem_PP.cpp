#include "BaseExplosiveItem_PP.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"

// Sets default values
ABaseExplosiveItem_PP::ABaseExplosiveItem_PP()
{
 	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	SetRootComponent(StaticMesh);
	StaticMesh->SetIsReplicated(true);

	ExplosionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("ExplosionSphere"));
	ExplosionSphere->SetupAttachment(StaticMesh);
	ExplosionSphere->SetVisibility(true);	
	ExplosionSphere->SetCollisionProfileName(UCollisionProfile::NoCollision_ProfileName);	
}

// Update SphereComponent in viewport
void ABaseExplosiveItem_PP::OnConstruction(const FTransform& Transform)
{
	ExplosionSphere->SetSphereRadius(DamageRadius);
}


// Called when the game starts or when spawned
void ABaseExplosiveItem_PP::BeginPlay()
{
	Super::BeginPlay();

	if (HasAuthority())
	{
		SetReplicates(true);
		SetReplicateMovement(true);
	}

	AActor* ExItem = this;
	if (ExItem != nullptr)
	{
		ExItem->OnTakeAnyDamage.AddDynamic(this, &ABaseExplosiveItem_PP::GetDamage);
	}	
}

// Binds
void ABaseExplosiveItem_PP::GetDamage(AActor* DamagedActor, float DamageTaken, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	ObjectExplode();
}

void ABaseExplosiveItem_PP::DestroyLogic()
{
	Destroy();
}


// Called every frame
void ABaseExplosiveItem_PP::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}


void ABaseExplosiveItem_PP::ObjectExplode()
{
	TArray <TEnumAsByte<EObjectTypeQuery>> ObjTypesArr;
	ObjTypesArr.Add(EObjectTypeQuery::ObjectTypeQuery3);

	UKismetSystemLibrary::SphereTraceMultiForObjects(this, GetActorLocation(), GetActorLocation(), DamageRadius, ObjTypesArr, false, ActorsToIgnore, EDrawDebugTrace::ForDuration, OutHits, true);

	for (FHitResult HitPlayers : OutHits)
	{
		ExplodedPlayers.AddUnique(Cast<ACharacter>(HitPlayers.GetActor()));		
	}

	for (ACharacter* ExplodedPlayer : ExplodedPlayers)
	{
		if (ExplodedPlayer != nullptr)
		{
			ExplodedPlayer->TakeDamage(Damage, FDamageEvent(), nullptr, this);
		}
	}

	ObjectFX();
		
}

void ABaseExplosiveItem_PP::ObjectFX_Implementation()
{
	StaticMesh->SetVisibility(false);

	UGameplayStatics::SpawnEmitterAtLocation(this, ExplodeParticle, GetActorLocation(), FRotator::ZeroRotator, FVector(4.f));
	UAudioComponent* ExlodeSound = UGameplayStatics::SpawnSoundAtLocation(this, DeathSound, GetActorLocation());

	if (ExlodeSound != nullptr)
	{
		ExlodeSound->OnAudioFinished.AddDynamic(this, &ABaseExplosiveItem_PP::DestroyLogic);
	}
}
