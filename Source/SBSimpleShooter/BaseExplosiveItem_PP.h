#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Containers/Array.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "BaseExplosiveItem_PP.generated.h"

UCLASS()
class SBSIMPLESHOOTER_API ABaseExplosiveItem_PP : public AActor
{
	GENERATED_BODY()
	
	float Damage = 100;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	float DamageRadius = 500;

	TArray <ACharacter*> ExplodedPlayers;
	TArray <AActor*> ActorsToIgnore;
	TArray <FHitResult> OutHits;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* StaticMesh;

	UPROPERTY()
	USphereComponent* ExplosionSphere;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Effects", meta = (AllowPrivateAccess = "true"))
	UParticleSystem* ExplodeParticle;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Effects", meta = (AllowPrivateAccess = "true"))
	USoundBase* DeathSound;

	
protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	void GetDamage(AActor* DamagedActor, float DamageTaken, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);
	

	UFUNCTION()
	void DestroyLogic();

	UFUNCTION()
	void ObjectExplode();

	UFUNCTION(NetMulticast, Reliable)
	void ObjectFX();

	UFUNCTION()
	virtual void OnConstruction(const FTransform& Transform) override;
		
	
public:	
	ABaseExplosiveItem_PP();

	virtual void Tick(float DeltaTime) override;	

};
